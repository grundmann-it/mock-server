import * as path from 'path';
import { readFile } from './file-system';
import expectedFileContent from './file-system.spec.json';

describe('readFile', () => {

  test('reads file from file system', async () => {
    // Arrange
    const filePath = path.join(__dirname, './file-system.spec.json');

    // Act
    const fileContent = await readFile(filePath);
    const actualFileContent = JSON.parse(fileContent);

    // Assert
    expect(actualFileContent).toEqual(expectedFileContent);
  });

  test('throws error if file not exists', async () => {
    // Arrange
    const filePath = path.join(__dirname, './non-existing-file.json');

    // Act & Assert
    expect(readFile(filePath)).rejects.toEqual(expect.any(Error));
  });

});
