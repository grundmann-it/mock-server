import * as fs from "fs";

export async function readFile(path: string): Promise<string> {
  return new Promise<any>((resolve, reject) => {
    if (fs.existsSync(path)) {
      fs.readFile(path, {encoding: 'utf8'}, (error, data) => {
        if (error != null) {
          reject(error);
        } else {
          resolve(data);
        }
      });
    } else {
      reject(new Error(`File "${path}" does not exist`));
    }
  });
}
