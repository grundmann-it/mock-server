import * as path from "path";
import { readFile } from '../data/file-system';

export enum HttpMethod {
  Get = 'GET'
}

export interface Header {
  name: string;
  value: string;
}

export interface Response {
  statusCode: number;
  body: any;
}

export interface Resource {
  id: string;
  method: HttpMethod;
  path: string;
  scenario: string;
}

export interface Config {
  headers: Header[];
  resources: Resource[];
}

export async function readConfig(configPath: string): Promise<Config> {
  try {
    const data = await readFile(configPath);
    return JSON.parse(data) as Config;

  } catch (e) {
    throw new Error(`Unable to load config "${configPath}", caused by\n\t> ${e}`);
  }
}

export async function readScenario(basePath: string, resourceId: string, scenario: string): Promise<Response> {
  const scenarioPath = path.join(basePath, 'scenarios', resourceId, `${scenario}.json`);

  try {
    const data = await readFile(scenarioPath);
    return JSON.parse(data) as Response;

  } catch (e) {
    throw new Error(`Unable to load scenario "${scenario}" for resource with id "${resourceId}", caused by\n\t> ${e}`);
  }
}
