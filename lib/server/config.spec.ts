import { readConfig, readScenario } from './config';
import * as path from 'path';
import expectedConfig from './config.spec.json';
import expectedResource from './scenarios/user/success.spec.json';

describe('Config', () => {

  describe('readConfig', () => {

    test('reads config from file system', async () => {
      // Arrange
      const configPath = path.join(__dirname, './config.spec.json');

      // Act
      const actualConfig = await readConfig(configPath);

      // Assert
      expect(actualConfig).toEqual(expectedConfig);
    });

    test('throws if config does not exists', async () => {
      // Arrange
      const configPath = path.join(__dirname, './non-existing-config.spec.json');

      // Act & Assert
      expect(readConfig(configPath)).rejects.toEqual(expect.any(Error));
    });

  });

  describe('readScenario', () => {

    test('reads scenario from file system', async () => {
      // Arrange
      const basePath = __dirname;
      const resourceId = 'user';
      const scenario = 'success.spec';

      // Act
      const actualScenario = await readScenario(basePath, resourceId, scenario);

      // Assert
      expect(actualScenario).toEqual(expectedResource);
    });

    test('throws if scenario does not exists', async () => {
      // Arrange
      const basePath = __dirname;
      const resourceId = 'token';
      const scenario = 'error';

      // Act & Assert
      expect(readScenario(basePath, resourceId, scenario)).rejects.toEqual(expect.any(Error));
    });

  });

});
