import { Preferences, isPreference, getPreferenceName, readPreferences, defaultPort } from './preferences';
import ProcessEnv = NodeJS.ProcessEnv;
import * as path from 'path';

const expectedPort = 4000;
const expectedPath = 'path/to/config';

const processEnv: ProcessEnv = {
  'npm_package_mockServer_port': `${expectedPort}`,
  'npm_package_mockServer_path': `./${expectedPath}`,
  'npm_package_license': 'MIT'
};

describe('Preferences', () => {

  describe('getPreferenceName', function () {

    test('gets preference name from object key', () => {
      // Act
      const actualSetting = getPreferenceName('npm_package_mockServer_port');

      // Assert
      expect(actualSetting).toEqual('port');
    });

  });

  describe('isPreference', function () {

    test('returns true if key matches any preference key', () => {
      // Act
      const actualSetting = isPreference('npm_package_mockServer_port');

      // Assert
      expect(actualSetting).toBe(true);
    });

    test('returns false if key does not match any preference key', () => {
      // Act
      const actualSetting = isPreference('npm_package_license');

      // Assert
      expect(actualSetting).toBe(false);
    });

  });

  describe('update', function () {

    test('updates preferences with another', () => {
      // Arrange
      const expectedPort = 1234;
      const expectedPath = './path/to/config';
      const config = new Preferences(expectedPort, './another/path');

      // Act
      const actualConfig = config.update({path: expectedPath});

      // Assert
      expect(actualConfig.port).toEqual(expectedPort);
      expect(actualConfig.path).toEqual(expectedPath);
      expect(actualConfig).not.toEqual(config);
    });

  });

  describe('readPreferences (package.json)', function () {
    let actualConfig: Preferences;

    beforeEach(() => {
      actualConfig = readPreferences(processEnv);
    });

    describe('port', function () {

      test('gets port', () => {
        // Assert
        expect(actualConfig.port).toEqual(expectedPort);
      });

      test('gets default port if not set', () => {
        // Arrange
        const processEnv = {};

        // Act
        actualConfig = readPreferences(processEnv);

        // Assert
        expect(actualConfig.port).toEqual(defaultPort);
      });

      test('gets default port if value is not a number', () => {
        // Arrange
        const processEnv = {
          'npm_package_mockServer_port': `test_port`
        };

        // Act
        actualConfig = readPreferences(processEnv);

        // Assert
        expect(actualConfig.port).toEqual(defaultPort);
      });

    });

    describe('path', function () {

      test('gets path', () => {
        // Arrange
        const expectedDir = path.join(process.env.INIT_CWD, expectedPath);

        // Assert
        expect(actualConfig.path).toEqual(expectedDir);
      });

    });
  });

});
