import ProcessEnv = NodeJS.ProcessEnv;
import * as _path from 'path';

const preferencePrefix = 'npm_package_mockServer_';

export const defaultPort = 4201;

/**
 * Tries to parse the given value to a number or returns
 * the default value if value is not a number
 *
 * @param value The value to parse
 * @param {number} defaultValue
 * @returns {number}
 */
function parseNumber(value: any, defaultValue: number): number {
  const number = parseInt(value, 10);

  return isNaN(number) ? defaultValue : number
}

function resolvePath(path: string): string {
  const basePath = process.env.INIT_CWD;
  return _path.join(basePath, path);
}

interface IPreferences {
  readonly port?: number;
  readonly path?: string;
}

export class Preferences implements IPreferences {

  constructor(public readonly port: number = defaultPort,
              public readonly path?: string) {}

  /**
   * Updates this preferences with the given preferences.
   * Returns a new instance instead of modifying the current instance.
   * @param {IPreferences} preferences
   * @returns {Preferences}
   */
  public update(preferences: IPreferences): Preferences {
    const {
      port = this.port,
      path = this.path
    } = preferences;

    return new Preferences(
      port,
      path
    );
  }

}

export function readPreferences(processEnv: ProcessEnv = process.env) {
  const entries = Object.entries(processEnv);

  return entries.reduce((config, entry) => {
    const [key, value] = entry;

    if (isPreference(key)) {
      const setting = getPreferenceName(key);

      switch (setting) {
        case 'port':
          const port = parseNumber(value, defaultPort);
          return config.update({port});

        case 'path':
          const path = resolvePath(value);
          return config.update({path});
      }
    }

    return config;
  }, new Preferences());
}

export function isPreference(key: string): boolean {
  return key.indexOf(preferencePrefix) !== -1;
}

export function getPreferenceName(key: string): string {
  return key.substring(preferencePrefix.length, key.length);
}
